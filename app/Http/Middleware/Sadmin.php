<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Sadmin
{
    public function handle(Request $request, Closure $next): Response
    {
        if(auth()->user()->is_admin == 'sadmin') return $next($request);
   
        return redirect('403')->with('error',"Hanya Admin Yang Bisa Masuk!!!");
    }
}
