<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Mahasiswa
{
    public function handle(Request $request, Closure $next): Response
    {
        if(auth()->user()->is_admin == 'mahasiswa') return $next($request);
   
        return redirect('403')->with('error',"Hanya Mahasiswa Yang Bisa Masuk!!!");
    }
}