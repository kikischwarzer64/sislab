<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function mahasiswa()
    {
        return view('users.welcome');
    }
    
    public function admin()
    {
        return view('users.welcome');
    }

    public function sadmin()
    {
        return view('users.welcome');
    }

    // public function role_home()
    // {
    //     if(Auth::user()->is_admin == 'admin')
    //     {
    //         return view(u)
    //     }
    // }
}
