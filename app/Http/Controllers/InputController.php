<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Barang;
use App\Models\User;
use App\Models\Pinjam;

class InputController extends Controller
{
    // Semua Fungsi untuk Peminjaman dan Pengembalian //
    public function add_peminjaman(Request $request)
    {
        Pinjam::create
        (
            [
                'name' => $request->input('nama'),
                'name_item' => $request->input('namabarang'),
                'type' => $request->input('tipe'),
                'condition' => $request->input('kondisi'),
                'quantity' => $request->input('qty'),
                'reason' => $request->input('alasan'),
                'isagree' => $request->input('aksi')
            ]
        );

        return redirect()->back()->with('status_input_peminjaman', 'Peminjaman');
    }

    public function update_peminjaman(Request $request, $id)
    {
        $update_pinjam = Pinjam::find($id);
        $update_pinjam->name = $request->input('nama');
        $update_pinjam->name_item = $request->input('namabarang');
        $update_pinjam->type = $request->input('tipe');
        $update_pinjam->condition = $request->input('kondisi');
        $update_pinjam->quantity = $request->input('qty');
        $update_pinjam->reason = $request->input('alasan');
        $update_pinjam->isagree = $request->input('aksi');
        $update_pinjam->update();

        return redirect()->back()->with('status_update_peminjaman', 'Barang Berhasil di Update');
    }



    // Semua Fungsi untuk Manajemen Barang //

    public function add_barang(Request $request)
    {
        Barang::create
        (
            [
                'name' => $request->input('nama'),
                'type' => $request->input('tipe'),
                'condition' => $request->input('kondisi'),
                'quantity' => $request->input('qty')
            ]
        );
        return redirect()->back()->with('status_input_barang', 'Barang nama Berhasil di Tambah');
    }

    public function read_barang()
    {
        $data_barang = Barang::all();
        $data_peminjaman = Pinjam::all();
        
        return view('users.dashboardAdmin', compact('data_barang', 'data_peminjaman'));
    }

    public function read_barang_mahasiswa()
    {
        $data_barang = Barang::all();
        $data_peminjaman = Pinjam::all();

        return view('users.dashboardMahasiswa', compact('data_barang', 'data_peminjaman'));
    }

    public function update_barang(Request $request, $id)
    {
        $update_barang = Barang::find($id);
        $update_barang->name = $request->input('nama');
        $update_barang->type = $request->input('tipe');
        $update_barang->condition = $request->input('kondisi');
        $update_barang->quantity = $request->input('qty');
        $update_barang->update();
        return redirect()->back()->with('status_update_barang','Barang Berhasil di Update');
    }

    public function delete_barang($id) {
        Barang::where('id', $id)->delete();
        
        return redirect()->back()->with('status_hapus_barang', 'Barang Berhasil di Hapus');
    }

    // Semua Fungsi untuk Manajemen User //
    
    public function add_user(Request $request)
    {
        User::create
        (
            [
                'name' => $request->input('nama'),
                'email' => $request->input('email'),
                'is_admin' => $request->input('role'),
                'password' => Hash::make($request->input('password')),
            ]
        );
        return redirect()->back()->with('status_input_user', 'User is_admin Berhasil di Tambah');
    }

    public function read_user()
    {
        $data_user = User::all();
        return view('users.dashboardSadmin', compact('data_user'));
    }

    public function update_user(Request $request, $id)
    {
        $update_user = User::find($id);
        $update_user->name = $request->input('nama');
        $update_user->email = $request->input('email');
        $update_user->is_admin = $request->input('role');
        $update_user->password = Hash::make($request->input('password'));
        $update_user->update();
        return redirect()->back()->with('status_update_user','User Berhasil di Update');
    }

    public function delete_user($id) 
    {
        User::where('id', $id)->delete();    
        return redirect()->back()->with('status_hapus_user', 'User Berhasil di Hapus');
    }
}
