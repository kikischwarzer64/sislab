{{-- <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SIJARANG') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <script src="https://kit.fontawesome.com/4bcadf6711.js" crossorigin="anonymous"></script>

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <i class="fa fa-box-open"></i> {{ config('app.name', 'SIJARANG ') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    
                    <ul class="navbar-nav ms-auto">
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif
                        @else
                            
                        @if (Auth::user()->is_admin == TRUE)
                            <li class="nav-item">
                                <a class="nav-link" href="/admin/tambah-data-barang"><i class="fa fa-book"></i> {{ __('Tambah Data Barang') }}</a>
                            </li>

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}"><i class="fa fa-user"></i> {{ __('Tambah Data Mahasiswa') }}</a>
                                </li>
                            @endif
                        @else
                            {{-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}"><i class="fa fa-book"></i> {{ __('Tambah') }}</a>
                            </li> --}}
                        {{-- @endif

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->email }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ __('Profile') }}
                                        <i class="fas fa-id-card"></i>
                                    </a>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">                                        
                                        {{ __(' Logout') }}
                                        <i class="fas fa-sign-out-alt"></i>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                                
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html> --}}

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>{{ config('app.name', 'SISLAB') }}</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link href="{{ url("/resources/css/styles.css") }}" rel="stylesheet" />
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
        <script src={{ url("/resources/js/alerto.js") }}></script>
        
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            
            @if(Auth::user()->is_admin == 'admin')
                <a class="navbar-brand ps-3" href="{{ url('/admin/home') }}"><i class="fa fa-box-open"></i> {{ __("SISLAB") }}</a>
            @elseif(Auth::user()->is_admin == 'sadmin')
                <a class="navbar-brand ps-3" href="{{ url('/sadmin/home') }}"><i class="fa fa-box-open"></i> {{ __("SISLAB") }}</a>
            @elseif(Auth::user()->is_admin == 'mahasiswa')
                <a class="navbar-brand ps-3" href="{{ url('/mahasiswa/home') }}"><i class="fa fa-box-open"></i> {{ __("SISLAB") }}</a>
            @endif

            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <div class="input-group"> </div>
            
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}" 
                                onclick="event.preventDefault(); 
                                document.getElementById('logout-form').submit();" 
                                {{ __('Logout') }}><i class="fa-solid fa-arrow-right-from-bracket fa-rotate-180"></i> Logout
                            </a>
                        </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            
                            <div class="sb-sidenav-menu-heading">{{ __("Home") }}</div>
                            
                            @if(Auth::user()->is_admin == 'admin')
                                <a class="nav-link" href="/admin/dashboard">
                                    <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                                    {{ __("Dashboard") }}
                                </a>
                            
                            @elseif(Auth::user()->is_admin == 'sadmin')
                                <a class="nav-link" href="/sadmin/dashboard">
                                    <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                                    {{ __("Dashboard") }}
                                </a>
                            
                            @elseif(Auth::user()->is_admin == 'mahasiswa')
                                <a class="nav-link" href="/admin/dashboard">
                                    <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                                    {{ __("Dashboard") }}
                                </a>
                            @endif   
                        </div>
                    </div>

                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        @if(Auth::user()->is_admin == 'admin')
                            {{ __(" Administrator ") }}
                        @elseif(Auth::user()->is_admin == 'sadmin')
                            {{ __(" Super Administrator ") }}
                        @elseif(Auth::user()->is_admin == 'mahasiswa')
                            {{ Auth::user()->name }}
                        @endif
                    </div>

                </nav>
            </div>

            <div id="layoutSidenav_content">
                
                <main>
                    @yield('content')
                </main>

                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; SISLAB Polindra 2023</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src={{ url("resources/js/scripts.js") }}></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src={{ url("resources/assets/demo/chart-area-demo.js") }}></script>
        <script src={{ url("resources/assets/demo/chart-bar-demo.js") }}></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
        <script src={{ url("resources/js/datatables-simple-demo.js") }}></script>
    </body>
</html>
