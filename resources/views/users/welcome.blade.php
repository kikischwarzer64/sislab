@extends('layouts.app')
@section('content')

    @if(session('status_login'))
        <script> showAlert("Berhasil Login", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @endif

<div class="container-fluid px-4">
    <h1 class="mt-4">
        @if(Auth::user()->is_admin == 'admin')
            {{ __("Selamat Datang Administrator") }}
        @elseif(Auth::user()->is_admin == 'sadmin')
            {{ __("Selamat Datang Super Administrator") }}
        @elseif(Auth::user()->is_admin == 'mahasiswa')
            {{ __("Selamat Datang") }} {{ Auth::user()->name }}
        @endif
    </h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Selamat Datang di Aplikasi Sistem Peminjaman Barang, 
            <code> Klik 
                    @if(Auth::user()->is_admin == 'admin')
                        <a class="btn btn-primary btn-sm" href="/admin/dashboard"> Dashboard </a> 
                    @elseif(Auth::user()->is_admin == 'sadmin')
                        <a class="btn btn-primary btn-sm" href="/sadmin/dashboard"> Dashboard </a> 
                    @elseif(Auth::user()->is_admin == 'mahasiswa')
                        <a class="btn btn-primary btn-sm" href="/mahasiswa/dashboard"> Dashboard </a> 
                    @endif
                    untuk memasuki menu utama
            </code>
        </li>
    </ol>
</div>

@endsection
