@extends('layouts.app')
@section('content')

<div class="container-fluid px-4">
    <h1 class="mt-4">{{ __("Tambah Data Mahasiswa") }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">{{ __("Dashboard") }}</a></li>
        <li class="breadcrumb-item active">{{ __("Tambah Data Mahasiswa") }}</li>
    </ol>
    <hr>

    

    <form method="POST" action="/sadmin/proses-user">
        
        @csrf

        <div class="row mb-3">
            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama: ') }}</label>
            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="nama" required autofocus />
            </div>
        </div>

        <div class="row mb-3">
            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('E-Mail: ') }}</label>
            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" required autofocus /> 
            </div>
        </div>

        <div class="row mb-3">
            <label for="is_admin" class="col-md-4 col-form-label text-md-end">{{ __('Role: ') }}</label>
            <div class="col-md-6">
                <input id="is_admin" type="radio" name="role" value="admin"/> Administrator
                <br>
                <input id="is_admin" type="radio" name="role" value="mahasisiwa"/> Mahasiswa
            </div>
        </div>

        <div class="row mb-3">
            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password: ') }}</label>
            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>
            </div>
        </div>

        <div class="row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" id="success" class="btn btn-primary">
                    {{ __('Tambah') }}
                </button>
            </div>
        </div>
    </form>
    <button type="submit" id="success" class="btn btn-primary">COBA</button>
    <hr>

    <div class="alert alert-success alert-solid" role="alert">This is a solid, success alert!</div>

</div>

@endsection