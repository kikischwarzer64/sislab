@extends('layouts.app')
@section('content')

{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Tambah Data Barang') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama Barang: ') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autofocus>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="type" class="col-md-4 col-form-label text-md-end">{{ __('Jenis Barang: ') }}</label>

                            <div class="col-md-6">
                                <input id="type" type="radio" name="tipe" value="1"/> Habis Pakai
                                <br>
                                <input id="type" type="radio" name="tipe" value="0"/> Non Habis Pakai
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="condition" class="col-md-4 col-form-label text-md-end">{{ __('Kondisi Barang: ') }}</label>

                            <div class="col-md-6">
                                <input id="condition" type="radio" name="kondisi" value="1"/> Rusak / Hilang
                                <br>
                                <input id="condition" type="radio" name="kondisi" value="0"/> Normal

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('Jumlah Barang: ') }}</label>

                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control" name="qty" required>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="container-fluid px-4">
    <h1 class="mt-4">{{ __("Tambah Data Barang") }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">{{ __("Dashboard") }}</a></li>
        <li class="breadcrumb-item active">{{ __("Tambah Data Barang") }}</li>
    </ol>
    <hr>

    <form method="POST" action="/admin/proses-barang">
        
        @csrf

        <div class="row mb-3">
            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama Barang: ') }}</label>
            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="nama" required autofocus>
            </div>
        </div>

        <div class="row mb-3">
            <label for="type" class="col-md-4 col-form-label text-md-end">{{ __('Jenis Barang: ') }}</label>
            <div class="col-md-6">
                <input id="type" type="radio" name="tipe" value="Habis Pakai"/> Habis Pakai
                <br>
                <input id="type" type="radio" name="tipe" value="Non Habis Pakai"/> Non Habis Pakai
            </div>
        </div>

        <div class="row mb-3">
            <label for="condition" class="col-md-4 col-form-label text-md-end">{{ __('Kondisi Barang: ') }}</label>
            <div class="col-md-6">
                <input id="condition" type="radio" name="kondisi" value="Rusak/Hilang"/> Rusak / Hilang
                <br>
                <input id="condition" type="radio" name="kondisi" value="Normal"/> Normal
            </div>
        </div>

        <div class="row mb-3">
            <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('Jumlah Barang: ') }}</label>
            <div class="col-md-6">
                <input id="quantity" type="text" class="form-control" name="qty" required>
            </div>
        </div>

        <div class="row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Tambah') }}
                </button>
            </div>
        </div>
    </form>
    <hr>
</div>
@endsection