@extends('layouts.app')
@section('content')

<div class="container-fluid px-4">
    <h1 class="mt-4">Dashboard</h1> <ol class="breadcrumb mb-4"> <li class="breadcrumb-item active">Dashboard</li> </ol>

    @if(session('status_input_barang'))
        <script> showAlert("Barang Berhasil Di Tambahkan", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @elseif(session('status_hapus_barang'))
        <script> showAlert("Barang Berhasil Di Hapus", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @elseif(session('status_update_peminjaman'))
        <script> showAlert("Peminjam Berhasil Di Update", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @endif

    <div class="row">

        <div class="col-xl-3 col-md-6">
            <div class="card bg-primary text-white mb-4">
                <div class="card-body">
                    @php $count_m_pinjam = 0 @endphp
                    @foreach ($data_peminjaman as $data)
                        @if($data->isagree == 'agree')
                            @php $count_m_pinjam++ @endphp
                        @endif
                    @endforeach

                    <h1> {{ $count_m_pinjam }} <i class="fa fa-user-group"></i></h1>
                    Mahasiswa Meminjam
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card bg-warning text-white mb-4">
                <div class="card-body">
                    @php $count_barang = 0 @endphp
                    @foreach ($data_barang as $barang)
                        @php $count_barang = $count_barang + $barang->quantity @endphp
                    @endforeach
                    <h1>{{ $count_barang }} <i class="fa fa-person-chalkboard"></i></i></h1>
                    Total Barang
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card bg-success text-white mb-4">
                <div class="card-body">
                    
                    @php $count_normal = 0 @endphp
                    @foreach ($data_barang as $barang)
                        @if($barang->condition == "Normal")
                            @php $count_normal = $count_normal + $barang->quantity @endphp
                        @endif
                    @endforeach

                    <h1>{{ $count_normal }} <i class="fa fa-box"></i> <i class="fa fa-check"></i></h1>
                    Total Barang Normal
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card bg-danger text-white mb-4">
                <div class="card-body">

                    @php $count_rusak = 0 @endphp
                    @foreach ($data_barang as $barang)
                        @if($barang->condition == "Rusak/Hilang")
                            @php $count_rusak = $count_rusak + $barang->quantity @endphp
                        @endif
                    @endforeach

                    <h1>{{ $count_rusak }} <i class="fa fa-box"></i> <i class="fa fa-xmark"></i></h1>
                    Total Barang Rusak / Hilang
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>

    </div>
    
    <div class="card mb-4">
        
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            {{ __("Data Barang") }}
        </div>

        <div class="card-body">
            <button class="btn btn-primary col-xl-2 mb-3 btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#add_barang"><i class="fas fa-user-plus"></i> Tambah Barang</button>
            <table id="data_barang">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @php $count_barang_table = 1 @endphp
                    @foreach ($data_barang as $barang_table)
                        <tr>
                            <td>{{ $count_barang_table++ }}</td>
                            <td>{{ $barang_table->name }}</td>
                            <td>{{ $barang_table->type }}</td>
                            <td>{{ $barang_table->condition }}</td>
                            <td>{{ $barang_table->quantity }}</td>
                            <td>
                                <button class="btn btn-warning form-control btn-sm" data-bs-toggle="modal" data-bs-target="#update_barang{{ $barang_table->id }}"><i class="fa-regular fa-pen-to-square"></i> Edit</button>
                                <button class="btn btn-danger form-control mt-2 btn-sm" data-bs-toggle="modal" data-bs-target="#confirmation{{ $barang_table->id }}"><i class="fa-solid fa-trash"></i> Hapus</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            {{ __("Data Peminjaman") }}
        </div>
        <div class="card-body">
            <table id="data_pinjam">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Mahasiswa</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Alasan Peminjaman</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama Mahasiswa</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Alasan Peminjaman</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @php $count_pinjam = 1 @endphp
                    @foreach ($data_peminjaman as $pinjam)
                    <tr>
                        <th>{{ $count_pinjam++ }}</th>
                        <th>{{ $pinjam->name }}</th>
                        <th>{{ $pinjam->name_item }}</th>
                        <th>{{ $pinjam->type }}</th>
                        <th>{{ $pinjam->condition }}</th>
                        <th>{{ $pinjam->quantity }}</th>
                        <th>{{ $pinjam->reason }}</th>
                        <th>
                            @if( $pinjam->isagree == 'agree' )
                                <button class="btn btn-warning form-control mt-2 btn-sm" data-bs-toggle="modal" data-bs-target="#confirmationh{{ $pinjam->id }}"><i class="fa-regular fa-trash"></i> Hapus</button>    
                                <button class="btn btn-success form-control mt-2 btn-sm" data-bs-toggle="modal" data-bs-target="#agree{{ $pinjam->id }}" disabled><i class="fa-regular fa-thumbs-up"></i> Terima</button>
                            @elseif($pinjam->isagree == 'disagree')
                                <button class="btn btn-warning form-control mt-2 btn-sm" data-bs-toggle="modal" data-bs-target="#confirmationh{{ $pinjam->id }}"><i class="fa-regular fa-trash"></i> Hapus</button>    
                                <button class="btn btn-danger form-control mt-2 btn-sm" data-bs-toggle="modal" data-bs-target="#disagree{{ $pinjam->id }}" disabled><i class="fa-regular fa-thumbs-down"></i> Tolak</button>
                            @else
                                <button class="btn btn-success form-control btn-sm" data-bs-toggle="modal" data-bs-target="#agree{{ $pinjam->id }}"><i class="fa-regular fa-thumbs-up"></i> Terima</button>
                                <button class="btn btn-danger form-control mt-2 btn-sm" data-bs-toggle="modal" data-bs-target="#disagree{{ $pinjam->id }}"><i class="fa-regular fa-thumbs-down"></i> Tolak</button>
                            @endif
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            {{ __("Data Pengembalian") }}
        </div>
        <div class="card-body">
            <table id="data_pengembalian">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Mahasiswa</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Alasan jika barang rusak / hilang</th>
                        <th>Jumlah Barang Normal</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama Mahasiswa</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Alasan jika barang rusak / hilang</th>
                        <th>Sisa Barang yang Normal</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <th>1</th>
                        <th>Kiki</th>
                        <th>Arduino</th>
                        <th>Non Habis Pakai</th>
                        <th>Normal</th>
                        <th>Mbleduk woy whwh</th>
                        <th>5</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Begin Modal Tambah Barang -->
    <div class="modal fade" id="add_barang" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Form Tambah Barang</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" action="/admin/add-barang">
        
                        @csrf
                
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="nama" required autofocus>
                            </div>
                        </div>
                
                        <div class="row mb-3">
                            <label for="type" class="col-md-4 col-form-label text-md-end">{{ __('Jenis Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="type" type="radio" name="tipe" value="Habis Pakai" checked/> Habis Pakai
                                <br>
                                <input id="type" type="radio" name="tipe" value="Non Habis Pakai"/> Non Habis Pakai
                            </div>
                        </div>
                
                        <div class="row mb-3">
                            <label for="condition" class="col-md-4 col-form-label text-md-end">{{ __('Kondisi Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="condition" type="radio" name="kondisi" value="Rusak/Hilang"/> Rusak / Hilang
                                <br>
                                <input id="condition" type="radio" name="kondisi" value="Normal" checked/> Normal
                            </div>
                        </div>
                
                        <div class="row mb-3">
                            <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('Jumlah Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control" name="qty" required>
                            </div>
                        </div>
                </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                            <button class="btn btn-primary btn-sm" type="submit">Kirim</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
    <!-- End Modal Tambah Barang -->

    <!-- Begin Modal Konfirmasi Hapus Barang -->
    @foreach ($data_barang as $barang_table_modal_confirmation)
        <div class="modal fade" id="confirmation{{$barang_table_modal_confirmation->id}}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Konfirmasi Hapus Barang</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin mau menghapus barang: <code class="text-danger">"{{ $barang_table_modal_confirmation->name }}"</code> ?....</p>
                        <p>Note: <code>Barang ini akan dihapus secara permanen!</code></p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                        <a class="btn btn-danger btn-sm" href="/admin/hapus-barang/{{ $barang_table_modal_confirmation->id }}">Hapus</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <!-- End Modal Konfirmasi Hapus Barang -->

    <!-- Begin Modal Agree -->
    @foreach ($data_peminjaman as $peminjaman_table_modal_confirmation)
        <div class="modal fade" id="agree{{$peminjaman_table_modal_confirmation->id}}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Konfirmasi Setuju Permintaan</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin anda menyutujui permintaan : <code class="text-danger">"{{ $peminjaman_table_modal_confirmation->name }}"</code> ?....</p>
                        
                        <form method="POST" action="/admin/update-peminjaman/{{ $peminjaman_table_modal_confirmation->id }}">
                            @csrf
                            <input type="hidden" name="nama" value='{{ $peminjaman_table_modal_confirmation->name}}' />
                            <input type="hidden" name="namabarang" value='{{ $peminjaman_table_modal_confirmation->name_item }}' />
                            <input type="hidden" name="tipe" value='{{ $peminjaman_table_modal_confirmation->type }}'/>
                            <input type="hidden" name="kondisi" value='{{ $peminjaman_table_modal_confirmation->condition }}'/>
                            <input type="hidden" name="qty" value='{{ $peminjaman_table_modal_confirmation->quantity }}'/>
                            <input type="hidden" name="alasan" value='{{ $peminjaman_table_modal_confirmation->reason }}'/>
                            <input type="hidden" name="aksi" value='agree'/>    
                    </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                            <button class="btn btn-danger btn-sm" type="submit">Setuju</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    <!-- End Modal Agree -->

    <!-- Begin Modal Disagree -->
    @foreach ($data_peminjaman as $peminjaman_table_modal_confirmation)
        <div class="modal fade" id="disagree{{$peminjaman_table_modal_confirmation->id}}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Konfirmasi Tifak Setuju Permintaan</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin anda tidak menyutujui permintaan : <code class="text-danger">"{{ $peminjaman_table_modal_confirmation->name }}"</code> ?....</p>
                        
                        <form method="POST" action="/admin/update-peminjaman/{{ $peminjaman_table_modal_confirmation->id }}">
                            @csrf
                            <input type="hidden" name="nama" value='{{ $peminjaman_table_modal_confirmation->name}}' />
                            <input type="hidden" name="namabarang" value='{{ $peminjaman_table_modal_confirmation->name_item }}' />
                            <input type="hidden" name="tipe" value='{{ $peminjaman_table_modal_confirmation->type }}'/>
                            <input type="hidden" name="kondisi" value='{{ $peminjaman_table_modal_confirmation->condition }}'/>
                            <input type="hidden" name="qty" value='{{ $peminjaman_table_modal_confirmation->quantity }}'/>
                            <input type="hidden" name="alasan" value='{{ $peminjaman_table_modal_confirmation->reason }}'/>
                            <input type="hidden" name="aksi" value='disagree'/>    
                    </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                            <button class="btn btn-danger btn-sm" type="submit">Tidak Setuju</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    <!-- End Modal Disagree -->

    <!-- Begin Modal Konfirmasi Edit Barang -->
    @foreach ($data_barang as $barang_table_modal_update)
    <div class="modal fade" id="update_barang{{ $barang_table_modal_update->id }}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Form Edit Barang</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" action="/admin/update-barang/{{ $barang_table_modal_update->id }}">
        
                        @csrf
                
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="nama" value='{{ $barang_table_modal_update->name }}' required autofocus>
                            </div>
                        </div>
                
                        <div class="row mb-3">
                            <label for="type" class="col-md-4 col-form-label text-md-end">{{ __('Jenis Barang: ') }}</label>
                            <div class="col-md-6">
                                @if($barang_table_modal_update->type == "Habis Pakai")
                                    <input id="type" type="radio" name="tipe" value="Habis Pakai" checked/> Habis Pakai
                                    <br>
                                    <input id="type" type="radio" name="tipe" value="Non Habis Pakai"/> Non Habis Pakai
                                @else
                                    <input id="type" type="radio" name="tipe" value="Habis Pakai"/> Habis Pakai
                                    <br>
                                    <input id="type" type="radio" name="tipe" value="Non Habis Pakai" checked/> Non Habis Pakai
                                @endif
                            </div>
                        </div>
                
                        <div class="row mb-3">
                            <label for="condition" class="col-md-4 col-form-label text-md-end">{{ __('Kondisi Barang: ') }}</label>
                            <div class="col-md-6">
                                @if($barang_table_modal_update->condition == "Normal")
                                    <input id="condition" type="radio" name="kondisi" value="Rusak/Hilang"/> Rusak / Hilang
                                    <br>
                                    <input id="condition" type="radio" name="kondisi" value="Normal" checked/> Normal
                                @else
                                    <input id="condition" type="radio" name="kondisi" value="Rusak/Hilang" checked/> Rusak / Hilang
                                    <br>
                                    <input id="condition" type="radio" name="kondisi" value="Normal"/> Normal
                                @endif
                            </div>
                        </div>
                
                        <div class="row mb-3">
                            <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('Jumlah Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control" name="qty" value='{{ $barang_table_modal_update->quantity }}' required>
                            </div>
                        </div>
                </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                            <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
    @endforeach
    <!-- End Modal Konfirmasi Edit Barang -->

        <!-- Begin Modal Konfirmasi Hapus peminjaman -->
    @foreach ($data_peminjaman as $pinjamh_table_modal_confirmation)
    <div class="modal fade" id="confirmationh{{$pinjamh_table_modal_confirmation->id}}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Konfirmasi Hapus Barang</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Yakin mau menghapus barang: <code class="text-danger">"{{ $pinjamh_table_modal_confirmation->name }}"</code> ?....</p>
                    <p>Note: <code>Barang ini akan dihapus secara permanen!</code></p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                    <a class="btn btn-danger btn-sm" href="/admin/hapus-peminjaman/{{ $pinjamh_table_modal_confirmation->id }}">Hapus</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <!-- End Modal Konfirmasi Hapus peminjaman -->


    <!-- Begin Modal Data Pengajuan Peminjaman -->
    <div class="modal fade" id="add_peminjaman" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Form Pengajuan Peminjaman</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" action="/admin/add-peminjaman">
        
                        @csrf
                
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama Anda: ') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="nama" required autofocus>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="namabarang" required autofocus>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="type" class="col-md-4 col-form-label text-md-end">{{ __('Jenis Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="type" type="radio" name="tipe" value="Habis Pakai" checked/> Habis Pakai
                                <br>
                                <input id="type" type="radio" name="tipe" value="Non Habis Pakai"/> Non Habis Pakai
                            </div>
                        </div>
                
                        {{-- <div class="row mb-3"> --}}
                            {{-- <label for="condition" class="col-md-4 col-form-label text-md-end">{{ __('Kondisi Barang: ') }}</label> --}}
                            {{-- <div class="col-md-6"> --}}
                                {{-- <input id="condition" type="hidden" name="kondisi" value="Rusak/Hilang"/> Rusak / Hilang --}}
                                {{-- <br> --}}
                                <input id="condition" type="hidden" name="kondisi" value="Normal" checked/> Normal
                            {{-- </div> --}}
                        {{-- </div> --}}
                
                        <div class="row mb-3">
                            <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('Jumlah Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control" name="qty" required>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('Alasan Peminjaman: ') }}</label>
                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control" name="alasan" required>
                            </div>
                        </div>
                </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                            <button class="btn btn-primary btn-sm" type="submit">Kirim</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
    <!-- End Modal Data Pengajuan Peminjaman -->





</div>
@endsection