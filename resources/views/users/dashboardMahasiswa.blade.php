@extends('layouts.app')
@section('content')

<div class="container-fluid px-4">
    <h1 class="mt-4">Dashboard</h1> <ol class="breadcrumb mb-4"> <li class="breadcrumb-item active">Dashboard</li> </ol>

    @if(session('status_input_barang'))
        <script> showAlert("Barang Berhasil Di Tambahkan", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @elseif(session('status_hapus_barang'))
        <script> showAlert("Barang Berhasil Di Hapus", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @elseif(session('status_update_barang'))
        <script> showAlert("Barang Berhasil Di Update", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @elseif(session('status_input_peminjaman'))
        <script> showAlert("Peminjaman Berhasil Di Ajuin", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @endif

    <div class="row">

        <div class="col-xl-6 col-md-6">
            <div class="card bg-primary text-white mb-4">
                <div class="card-body">
                    @php $count_pinjam_m = 0 @endphp
                    @foreach ($data_peminjaman as $data)
                        @if($data->name == Auth::user()->name)
                            @php $count_pinjam_m = $count_pinjam_m + $data->quantity @endphp
                        @endif
                    @endforeach
                    <h1> {{ $count_pinjam_m }} <i class="fa fa-user-group"></i></h1>
                    Barang di Pinjam
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-md-6">
            <div class="card bg-warning text-white mb-4">
                <div class="card-body">
                    @php $count_barang = 0 @endphp
                    @foreach ($data_barang as $barang)
                        @php $count_barang = $count_barang + $barang->quantity @endphp
                    @endforeach
                    <h1>{{ $count_barang }} <i class="fa fa-person-chalkboard"></i></i></h1>
                    Total Barang di LAB
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>

        

    </div>
    
    <div class="card mb-4">
        
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            {{ __("Data Barang") }}
        </div>

        <div class="card-body">
            <table id="data_barang">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @php $count_barang_table = 1 @endphp
                    @foreach ($data_barang as $barang_table)
                        <tr>
                            <td>{{ $count_barang_table++ }}</td>
                            <td>{{ $barang_table->name }}</td>
                            <td>{{ $barang_table->type }}</td>
                            <td>{{ $barang_table->condition }}</td>
                            <td>{{ $barang_table->quantity }}</td>
                            <td>
                                <button class="btn btn-warning form-control btn-sm" data-bs-toggle="modal" data-bs-target="#update_barang{{ $barang_table->id }}"><i class="fa-regular fa-pen-to-square"></i> Pinjam</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            {{ __("Data Peminjaman") }}
        </div>
        <div class="card-body">
            <table id="data_pinjam">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Mahasiswa</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Alasan Peminjaman</th>
                        <th>Tanggal Pinjam</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama Mahasiswa</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Alasan Peminjaman</th>
                        <th>Tanggal Pinjam</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @php $count_pinjam = 1 @endphp
                    @foreach ($data_peminjaman as $pinjam)
                    @if($pinjam->name == Auth::user()->name)
                        <tr>
                            <th>{{ $count_pinjam++ }}</th>
                            <th>{{ $pinjam->name }}</th>
                            <th>{{ $pinjam->name_item }}</th>
                            <th>{{ $pinjam->type }}</th>
                            <th>{{ $pinjam->condition }}</th>
                            <th>{{ $pinjam->quantity }}</th>
                            <th>{{ $pinjam->reason }}</th>
                            <th>{{ $pinjam->created_at }}</th>
                            <th>
                                @if($pinjam->isagree == 'agree')
                                    <button class="btn btn-success form-control btn-sm" disabled>Di Terima</button>
                                @elseif($pinjam->isagree == 'disagree')
                                    <button class="btn btn-danger form-control btn-sm" disabled>Di Tolak</button>
                                @else
                                    <button class="btn btn-warning form-control btn-sm" disabled>Belum Di Periksa</button>
                                @endif
                            </th>
                        </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            {{ __("Data Pengembalian") }}
        </div>
        <div class="card-body">
            <table id="data_pengembalian">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Mahasiswa</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Alasan jika barang rusak / hilang</th>
                        <th>Tanggal Pinjam</th>
                        <th>Jumlah Barang Normal</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama Mahasiswa</th>
                        <th>Nama Barang</th>
                        <th>Jenis Barang</th>
                        <th>Kondisi</th>
                        <th>Jumlah Barang</th>
                        <th>Alasan jika barang rusak / hilang</th>
                        <th>Tanggal Pinjam</th>
                        <th>Sisa Barang yang Normal</th>
                    </tr>
                </tfoot>
                <tbody>
                    @php $count_pinjama = 1 @endphp
                    @foreach ($data_peminjaman as $pinjam)
                    @if($pinjam->name == Auth::user()->name)
                        <tr>
                            <th>{{ $count_pinjama++ }}</th>
                            <th>{{ $pinjam->name }}</th>
                            <th>{{ $pinjam->name_item }}</th>
                            <th>{{ $pinjam->type }}</th>
                            <th>{{ $pinjam->condition }}</th>
                            <th>{{ $pinjam->quantity }}</th>
                            <th>{{ $pinjam->reason }}</th>
                            <th>{{ $pinjam->created_at }}</th>
                            <th>
                                <button class="btn btn-success form-control mt-2 btn-sm">Kembalikan</button>
                            </th>
                        </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Begin Modal Konfirmasi Hapus Barang -->
    @foreach ($data_barang as $barang_table_modal_confirmation)
        <div class="modal fade" id="confirmation{{$barang_table_modal_confirmation->id}}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Konfirmasi Hapus Barang</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Yakin mau menghapus barang: <code class="text-danger">"{{ $barang_table_modal_confirmation->name }}"</code> ?....</p>
                        <p>Note: <code>Barang ini akan dihapus secara permanen!</code></p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                        <a class="btn btn-danger btn-sm" href="/admin/hapus-barang/{{ $barang_table_modal_confirmation->id }}">Hapus</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <!-- End Modal Konfirmasi Hapus Barang -->

    <!-- Begin Modal Konfirmasi Pinjaman Barang -->
    @foreach ($data_barang as $barang_table_modal_pinjam)
    <div class="modal fade" id="update_barang{{ $barang_table_modal_pinjam->id }}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Form Edit Barang</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p> Nama: {{ Auth::user()->name }} </p>
                    <p> Nama Barang : {{ $barang_table_modal_pinjam->name }} </p>
                    <p> Tipe : {{ $barang_table_modal_pinjam->type }} </p>
                    <p> Kondisi : {{ $barang_table_modal_pinjam->condition }} </p>
                    <p> Jumlah : {{ $barang_table_modal_pinjam->quantity }} </p>
                    <p> Anda yakin ini yang mau di pinjam? </p>
                    <p> Tolong beri alasan kenapa anda mau meminjam alat / benda ini? </p>
                    
                    <form method="POST" action="/mahasiswa/add-peminjaman">
                        @csrf
                            <input type="hidden" name="nama" value='{{ Auth::user()->name }}'>
                            <input type="hidden" name="namabarang" value='{{ $barang_table_modal_pinjam->name }}'>
                            <input type="hidden" name="tipe" value='{{ $barang_table_modal_pinjam->type }}'/>
                            <input type="hidden" name="kondisi" value='{{ $barang_table_modal_pinjam->condition }}'/>
                            <input type="hidden" name="qty" value='{{ $barang_table_modal_pinjam->quantity }}'/>
                            <input type="hidden" name="aksi" value='notyet'/>
                        
                            <div class="row mb-3">
                            <label for="alasan" class="col-md-4 col-form-label text-md-end">{{ __('Alasan : ') }}</label>
                            <div class="col-md-6">
                                <input id="alasan" type="text" class="form-control" name="alasan" required autofocus>
                            </div>
                        </div>
                </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                            <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
                        </div>
                    </form>

            </div>
        </div>
    </div>
    @endforeach
    <!-- End Modal Konfirmasi Pinjaman Barang -->

    <!-- Begin Modal Data Pengajuan Peminjaman -->
    <div class="modal fade" id="add_peminjaman" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Form Pengajuan Peminjaman</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    
                    <form method="POST" action="/admin/add-peminjaman">
        
                        @csrf
                
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama Anda: ') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="nama" required autofocus>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nama Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="namabarang" required autofocus>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="type" class="col-md-4 col-form-label text-md-end">{{ __('Jenis Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="type" type="radio" name="tipe" value="Habis Pakai" checked/> Habis Pakai
                                <br>
                                <input id="type" type="radio" name="tipe" value="Non Habis Pakai"/> Non Habis Pakai
                            </div>
                        </div>
                
                        {{-- <div class="row mb-3"> --}}
                            {{-- <label for="condition" class="col-md-4 col-form-label text-md-end">{{ __('Kondisi Barang: ') }}</label> --}}
                            {{-- <div class="col-md-6"> --}}
                                {{-- <input id="condition" type="hidden" name="kondisi" value="Rusak/Hilang"/> Rusak / Hilang --}}
                                {{-- <br> --}}
                                <input id="condition" type="hidden" name="kondisi" value="Normal" checked/> Normal
                            {{-- </div> --}}
                        {{-- </div> --}}
                
                        <div class="row mb-3">
                            <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('Jumlah Barang: ') }}</label>
                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control" name="qty" required>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="quantity" class="col-md-4 col-form-label text-md-end">{{ __('Alasan Peminjaman: ') }}</label>
                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control" name="alasan" required>
                            </div>
                        </div>
                </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                            <button class="btn btn-primary btn-sm" type="submit">Kirim</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
    <!-- End Modal Data Pengajuan Peminjaman -->

</div>
@endsection