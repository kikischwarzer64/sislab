@extends('layouts.app')
@section('content')

<div class="container-fluid px-4">
    <h1 class="mt-4">Dashboard</h1><ol class="breadcrumb mb-4"><li class="breadcrumb-item active">Dashboard</li></ol>

    @if(session('status_input_user'))
        <script> showAlert("User Berhasil Di Tambahkan", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @elseif(session('status_hapus_user'))
        <script> showAlert("User Berhasil Di Hapus", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @elseif(session('status_update_user'))
        <script> showAlert("User Berhasil Di Update", 1000, 'success', 'fade-in-in', '1s', 'bottom'); </script>
    @endif

    <div class="row">
        <div class="col-xl-6 col-md-6">
            <div class="card bg-primary text-white mb-4">
                <div class="card-body">
                    @php $count_mahasiswa = 0 @endphp
                    @foreach ($data_user as $users)
                        @if($users->is_admin == 'mahasiswa')
                            @php $count_mahasiswa++ @endphp
                        @endif
                    @endforeach
                    <h1>{{ $count_mahasiswa }} <i class="fa fa-user-group"></i></h1>
                    {{ __('Mahasiswa terdaftar') }}
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-md-6">
            <div class="card bg-success text-white mb-4">
                <div class="card-body">
                    @php $count_admin = 0 @endphp
                    @foreach ($data_user as $users)
                        @if($users->is_admin == 'admin')
                            @php $count_admin++ @endphp
                        @endif
                    @endforeach
                    <h1>{{ $count_admin }} <i class="fa fa-user-plus"></i></i></h1>
                    {{ __('Admin Terdaftar') }}
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View Details</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            {{ __("Data Mahasiswa") }}
        </div>
        
        <div class="card-body">
            <button class="btn btn-primary col-xl-2 mb-2" type="button" data-bs-toggle="modal" data-bs-target="#add_users"><i class="fas fa-user-plus"></i> Tambah User</button>
            <table id="data_mahasiswa">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $count_user = 0 @endphp
                    @foreach ($data_user as $users_table)
                        @php $count_user++ @endphp
                    <tr>
                        <td>{{ $count_user }}</td>
                        <td>{{ $users_table->name }}</td>
                        <td>{{ $users_table->email }}</td>
                            
                            @if($users_table->is_admin == 'admin')
                                <td> 
                                    <div class="badge bg-warning text-black rounded-pill"> 
                                        Admin 
                                    </div>
                                </td>

                            @elseif($users_table->is_admin == 'sadmin')
                                
                                <td> 
                                    <div class="badge bg-primary text-white rounded-pill"> 
                                        Super Admin 
                                    </div>
                                </td>
                            
                            @elseif($users_table->is_admin == 'mahasiswa')
                                
                                <td>
                                    <div class="badge bg-success text-white rounded-pill"> 
                                        Mahasiswa 
                                    </div>
                                </td>
                                
                            @endif
                            <td>
                                <button class="btn btn-warning form-control mt-2" data-bs-toggle="modal" data-bs-target="#update_user{{ $users_table->id }}"><i class="fa-regular fa-pen-to-square"></i> Edit </button>
                                
                                @if($users_table->is_admin == 'sadmin')
                                    <button class="btn btn-danger form-control mt-2" data-bs-toggle="modal" data-bs-target="#confirmation{{ $users_table->id }}" disabled><i class="fa-solid fa-trash"></i> Hapus</button>
                                @else
                                    <button class="btn btn-danger form-control mt-2" data-bs-toggle="modal" data-bs-target="#confirmation{{ $users_table->id }}"><i class="fa-solid fa-trash"></i> Hapus</button>
                                @endif
                                
                            </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>        
    </div>

    <!-- Begin Modal Tambah User -->
    <div class="modal fade" id="add_users" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Form Tambah User</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/sadmin/proses-user">
                        @csrf
                        <div class="mb-3">
                            <div class="mb-3"><label for="name">Nama:</label>
                                <input class="form-control" id="name" type="name" name="nama" placeholder="abdul....">
                            </div>
                        </div>
                
                        <div class="mb-3">
                            <div class="mb-3"><label for="email">E-Mail:</label>
                                <input class="form-control" id="email" type="email" name="email" placeholder="abdul@gmail.com">
                            </div>
                        </div>
                
                        <div class="mb-3">
                            <div class="mb-3"><label for="password">Password:</label>
                                <input class="form-control" id="password" type="password" name="password" placeholder="************">
                            </div>
                        </div>

                        <div class="mb-3">
                            <div class="mb-3">
                                <label for="is_admin" >{{ __('Role: ') }}</label>
                                <div class="form-check">
                                    <input class="form-check-input" id="flexRadioDefault1" type="radio" name="role" value="mahasiswa" checked>
                                    <label class="form-check-label" for="flexRadioDefault1">Mahasiswa</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="flexRadioDefault2" type="radio" name="role" value="admin">
                                    <label class="form-check-label" for="flexRadioDefault2">Administrator</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="flexRadioDefault3" type="radio" name="role" value="sadmin" disabled>
                                    <label class="form-check-label" for="flexRadioDefault3">Super Administrator</label>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                    <button class="btn btn-primary" type="submit">Kirim</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal Tambah User -->

    <!-- Begin Modal Konfirmasi Hapus User -->
    @foreach ($data_user as $users_table_modal_confirmation)
    <div class="modal fade" id="confirmation{{$users_table_modal_confirmation->id}}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Konfirmasi Hapus User</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Yakin mau menghapus akun: <code class="text-danger">"{{ $users_table_modal_confirmation->name }}"</code> ?....</p>
                    <p>Note: <code>Akun ini akan dihapus secara permanen!</code></p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary btn-sm" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                    <a class="btn btn-danger btn-sm" href="/sadmin/hapus-user/{{ $users_table_modal_confirmation->id }}">Hapus</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <!-- End Modal Konfirmasi Hapus User -->

    <!-- Begin Modal Konfirmasi Edit User -->
    @foreach ($data_user as $users_table_modal_update)
    <div class="modal fade" id="update_user{{ $users_table_modal_update->id }}" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Form Tambah User</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/sadmin/update-user/{{ $users_table_modal_update->id }}">
                        @csrf
                        <div class="mb-3">
                            <div class="mb-3"><label for="name">Nama:</label>
                                <input class="form-control" id="name" type="name" name="nama" value='{{ $users_table_modal_update->name }}'>
                            </div>
                        </div>
                
                        <div class="mb-3">
                            <div class="mb-3"><label for="email">E-Mail:</label>
                                <input class="form-control" id="email" type="email" name="email" value='{{ $users_table_modal_update->email }}'>
                            </div>
                        </div>
                
                        <div class="mb-3">
                            <div class="mb-3"><label for="password">Password:</label>
                                <input class="form-control" id="password" type="password" name="password" placeholder="***************">
                            </div>
                        </div>

                        <div class="mb-3">
                            <div class="mb-3">
                                <label for="is_admin" >{{ __('Role: ') }}</label>
                                <div class="form-check">
                                    <input class="form-check-input" id="flexRadioDefault1" type="radio" name="role" value="mahasiswa" checked>
                                    <label class="form-check-label" for="flexRadioDefault1">Mahasiswa</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="flexRadioDefault2" type="radio" name="role" value="admin">
                                    <label class="form-check-label" for="flexRadioDefault2">Administrator</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="flexRadioDefault3" type="radio" name="role" value="sadmin" disabled>
                                    <label class="form-check-label" for="flexRadioDefault3">Super Administrator</label>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Tutup</button>
                    <button class="btn btn-primary" type="submit">Kirim</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach
    <!-- End Modal Konfirmasi Edit User -->
    
</div>

@endsection
