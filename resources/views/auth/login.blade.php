@extends('layouts.appLogin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @if(session('error'))
                <script>showAlert('Maaf, email dan password anda salah!', 1000, 'danger', 'fade-in-in', '1s', 'bottom');</script>
            @endif

            <div class="col-lg-4">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header"><h3 class="text-center font-weight-light my-2">{{ __("Log in") }}</h3></div>
                    <div class="card-body">
                        
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputEmail" name="email" type="email" {{-- value="{{ old('email') }}"--}} required autofocus/>
                                <label for="inputEmail">Email address</label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-floating mb-3">
                                <input class="form-control" id="inputPassword" type="password" name="password" required/>
                                <label for="inputPassword">Password</label>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-check mb-3">
                                <input class="form-check-input" id="inputRememberPassword" type="checkbox" value="" name="remember" {{ old('remember') ? 'checked' : '' }}/>
                                <label class="form-check-label" for="inputRememberPassword">Ingatkan Saya</label>
                            </div>
                            <div class="d-flex align-items-center justify-content-between mt-0 mb-2">
                                <button type="submit" class="btn btn-primary form-control"> {{ __('Login') }} </button>                                

                            </div>
                            <div class="d-flex align-items-center justify-content-between mt-0 mb-2">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-warning form-control" href="{{ route('password.request') }}">
                                        {{ __('Lupa Password?') }}
                                    </a>
                                @endif

                            </div>
                        </form>

                    </div>
                    <div class="card-footer text-center py-3">
                        <div class="small">{{ __("Copyright") }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
