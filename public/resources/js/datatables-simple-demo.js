window.addEventListener('DOMContentLoaded', event => {
    // Simple-DataTables
    // https://github.com/fiduswriter/Simple-DataTables/wiki

    const data_barang = document.getElementById('data_barang');
    if (data_barang) {
        new simpleDatatables.DataTable(data_barang);
    }
    
    const data_pinjam = document.getElementById('data_pinjam');
    if (data_pinjam) {
        new simpleDatatables.DataTable(data_pinjam);
    }

    const data_mahasiswa = document.getElementById('data_mahasiswa');
    if (data_mahasiswa) {
        new simpleDatatables.DataTable(data_mahasiswa);
    }

    const data_pengembalian = document.getElementById('data_pengembalian');
    if (data_pengembalian) {
        new simpleDatatables.DataTable(data_pengembalian);
    }
});
