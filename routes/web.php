<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InputController;
use App\Http\Controllers\Auth\LoginController;

Route::redirect('/', '/login', 301);

Route::redirect('/home', 'login', 301);

Auth::routes();

Route::get('/403', function()
{
    return view('errors.403');
});

Route::middleware(['auth', 'admin'])->group(function ()
{
    Route::get('/admin/home', 'HomeController@admin')->name('admin.route')->middleware('admin');
    Route::get('/admin/dashboard', [InputController::class, 'read_barang']);
    Route::post('/admin/add-barang', [InputController::class, 'add_barang']);
    Route::post('/admin/update-barang/{id}', [InputController::class, 'update_barang']);
    Route::get('/admin/hapus-barang/{id}', 'InputController@delete_barang');
    Route::get('/admin/hapus-peminjaman/{id}', 'InputController@delete_peminjaman');
    Route::post('/admin/update-peminjaman/{id}', [InputController::class, 'update_peminjaman']);
});

Route::middleware(['auth', 'mahasiswa'])->group(function ()
{
    Route::get('/mahasiswa/home', 'HomeController@mahasiswa')->name('mahasiswa.route')->middleware('mahasiswa');
    Route::get('/mahasiswa/dashboard', [InputController::class, 'read_barang_mahasiswa']);
    Route::post('/mahasiswa/add-peminjaman', [InputController::class, 'add_peminjaman']);
});

Route::middleware(['auth', 'sadmin'])->group(function ()
{
    Route::get('/sadmin/home', 'HomeController@sadmin')->name('sadmin.route')->middleware('sadmin');
    Route::get('/sadmin/dashboard', [InputController::class, 'read_user']);
    Route::post('/sadmin/proses-user', [InputController::class, 'add_user']);
    Route::post('/sadmin/update-user/{id}', [InputController::class, 'update_user']);
    Route::get('/sadmin/hapus-user/{id}', 'InputController@delete_user');
});




