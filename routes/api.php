<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ApiAuthController;
use App\Http\Controllers\API\ApiInputController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/login', [ApiAuthController::class, 'login']);

Route::group(['middleware' => 'auth:api',], function ()
{
    Route::get('item', [ApiAuthController::class, 'index'])->name('api.item.index');

    Route::get('/read', [ApiInputController::class, 'read_barang']);
    Route::get('/read-pinjam', [ApiInputController::class, 'read_peminjaman']);
    Route::post('/add-pinjam/{id}', [ApiInputController::class, 'add_peminjaman']);
    Route::put('/update-pinjam/{idp}/{idb}', [ApiInputController::class, 'update_peminjaman']);
    Route::delete('/delete-pinjam/{id}', [ApiInputController::class, 'delete_peminjaman']);
});